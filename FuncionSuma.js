//Nombre de la clase en mayuscula
class CalculadorAritmetico{
    //Variables ó atributos
    numero1;
    numero2;
    //Funciones (métodos) dentro de la clase
    sumar(){
        this.numero1=Number(document.getElementById('numero1').value);
        this.numero2=Number(document.getElementById('numero2').value);
        let txtResultado=document.getElementById("resultado");
        txtResultado.value=this.numero1+this.numero2;
        return true;
    } 
    restar(){
        this.numero1=Number(document.getElementById('numero1').value);
        this.numero2=Number(document.getElementById('numero2').value);
        let txtResultado=document.getElementById("resultado");
        txtResultado.value=this.numero1-this.numero2;
        return true;
    }
    modular(){
        this.numero1=Number(document.getElementById('numero1').value);
        this.numero2=Number(document.getElementById('numero2').value);
        let txtResultado=document.getElementById("resultado");
        txtResultado.value=this.numero1%this.numero2;
        return true;
    }

} 
//Objeto apartir de la clase
let miCalculadora= new CalculadorAritmetico();

class Calculador{
    //Variables ó atributos
    numero1;
    numero2;
    //Funciones (métodos) dentro de la clase
    potenciar(){
        this.numero1=Number(document.getElementById('numero1').value);
        this.numero2=Number(document.getElementById('numero2').value);
        let txtResultado=document.getElementById("resultado");
        let pontenciado=this.numero1;
        let i;
        for (i=0;i<(this.numero2)-1;i++){
            pontenciado=pontenciado*this.numero1;
        }
        txtResultado.value=pontenciado;
        return true
    }
    logx(){
        this.numero1=Number(document.getElementById('numero1').value);
        let txtResultado=document.getElementById("resultado");
        txtResultado.value=Math.log(this.numero1);
        return true
    }
    logy(){
        this.numero2=Number(document.getElementById('numero2').value);
        let txtResultado=document.getElementById("resultado");
        txtResultado.value=Math.log(this.numero2);
        return true
    }

} 
//Objeto apartir de la clase
let miCalculador= new Calculador();
